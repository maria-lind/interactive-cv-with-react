# README #

This project is based on [Create React App ](https://facebook.github.io/react/blog/2016/07/22/create-apps-with-no-configuration.html) and can either be run from cmd with npm start (dev setup) or by using the static files in build folder (production).

### What is this repository for? ###

* This is a quick React SPA. An interactive CV where user can see detailed view of listed life events 
* Version : 1.0

###This project uses###
* [Create React App](https://facebook.github.io/react/blog/2016/07/22/create-apps-with-no-configuration.html) as template
* ReactCSSTransitionGroup addon for animations
* Google Material Design for color scheme, animation guidelines, fonts and icons

### How do I get set up? ###

* Use the build folder for production
* Or running npm start in cmd (dev setup)

### Who do I talk to? ###

* @inaboxdesign
* maria.anna.lind@gmail.com