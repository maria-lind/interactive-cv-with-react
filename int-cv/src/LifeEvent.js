import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group' // ES6

//LifeEvent class provides basic constructor with event state (short)
//LifeEvent class provides showDetails function used to change state
class LifeEvent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {showingDetails: false};
    }
    
    showDetails() {
        this.setState(prevState => ({
              showingDetails: !prevState.showingDetails
        }));
    };
};

//EducationEvent, EmploymentEvent, and ProjectEvent use LifeEvent as basis
//Used for rendering of Events depending on type
export class EducationEvent extends LifeEvent {
    render() {

        //details element created as variable
        //to be used in ReactCSSTransitionGroup
        let details = null;
        if (this.state.showingDetails) {
            details = <div className="lifeEventDetails" key={this.props.itemId}> {this.props.details}</div>;
        }

        return (
            <li onClick={this.showDetails.bind(this)}>
                <i className={this.state.showingDetails ? "material-icons isRotated" : "material-icons"}>add</i>
                <div className="lifeEventContent">
                    <h3>{this.props.date} - {this.props.title}</h3>
                    <ReactCSSTransitionGroup component="div" transitionName="details" transitionEnterTimeout={300} transitionLeaveTimeout={200}>{details}</ReactCSSTransitionGroup>
                </div>
            </li>
        );
    }
};

export class EmploymentEvent extends LifeEvent {
    render () {

        //details element created as variable
        //to be used in ReactCSSTransitionGroup
        let details = null;
        if (this.state.showingDetails) {
            details = <div className="lifeEventDetails" key={this.props.itemId}> {this.props.details}</div>;
        }

        return (
            <li onClick={this.showDetails.bind(this)}>
                <i className={this.state.showingDetails ? "material-icons isRotated" : "material-icons"}>add</i>
                <div className="lifeEventContent">
                    <h3>{this.props.date} - {this.props.title} - {this.props.company}</h3>
                    <ReactCSSTransitionGroup component="div" transitionName="details" transitionEnterTimeout={300} transitionLeaveTimeout={200}>{details}</ReactCSSTransitionGroup>
                </div>
            </li>
        )
    }
};

export class ProjectEvent extends LifeEvent {
    render () {

        //details element created as variable
        //to be used in ReactCSSTransitionGroup
        let details = null;
        if (this.state.showingDetails) {
            details = <div className="lifeEventDetails" key={this.props.itemId}> {this.props.details}</div>;
        }

        return (
            <li onClick={this.showDetails.bind(this)}>
                <i className={this.state.showingDetails ? "material-icons isRotated" : "material-icons"}>add</i>
                <div className="lifeEventContent">
                    <h3>{this.props.date} - {this.props.title}</h3>
                   <ReactCSSTransitionGroup component="div" transitionName="details" transitionEnterTimeout={300} transitionLeaveTimeout={200}>{details}</ReactCSSTransitionGroup>
                </div>
            </li>
        )
    }
};

//Used for rendering skills
//Has no detailed view
export class Skill extends React.Component {
    render () {
        return <li className="skill">{this.props.skill}</li>
    }
};

