import React from 'react';

class CVContact extends React.Component {
    render () {

        //temp datasource
        const contact = {
            street      : "J.P.Larsens vej 48",
            zipcode     : "8220",
            city        : "Brabrand",
            phone       : "60 81 81 32",
            email       : "maria.anna.lind@gmail.com", 
        }

        return (
            <div className="contactContent">
                <ul>
                    <li>{contact.street}</li>
                    <li>{contact.zipcode}, {contact.city}</li> 
                </ul>
                <ul>
                    <li>{contact.phone}</li>
                    <li>{contact.email}</li>
                </ul>
            </div>
        )
    }
}

export default CVContact;