import React from 'react';
import {EducationEvent, EmploymentEvent, ProjectEvent, Skill} from './LifeEvent';

//CVList class uses different event types to render data
class CVList extends React.Component {
    render () {

        //Temp data sources
        const educationEvents = [
            {title : 'Highschool', date : '1999-2004', details : 'Highschool diploma with French Language and Literature. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'},
            {title : 'University of Westminster', date : '2006-2009', details : 'BA (Hons) French and International Relations. BA paper on the topic of multiculturalism and civil society.'},
            {title : 'BAA', date : '2010-2013', details : 'PBA in Web development. Neque porro quisquam est. Adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam. Quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?'},
        ];

        const employmentEvents = [
            {title : 'Intern and Student Programmer', company : "ProCore ApS", date : '2012-2013', details : 'I worked as front-end developer, primarily in Umbraco (HTML / CSS / jQuery / JavaScript / MVC / Razor)'},
            {title : 'Frontend Developer', company: "Kraftværk", date : '2013-2015', details : 'Frontend developer and consultant. Responsible not only for development but also for communicating with customers and advising them on choosing technical solutions. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?'},
            {title : 'Frontend Developer', company: "ProCore ApS", date : '2015-present', details : 'Webdeveloper working with Umbraco, Razor, C#, JS, AngularJS, HTML5, SCSS, gulp and git'},
        ];

        const projects = [
            {title : 'Jemogfix', date : '2013-2014', details : 'The project was originally based on Sitecore but in December 2013 it was migrated to Umbraco. The shop is custom build and integrated with the quickpay payment provider.'},
            {title : 'Homenet', date : '2014', details : 'Intranet for Home A/S based on Umbraco.'},
            {title : 'ProTravel', date : '2013', details : 'Software for small and medium travel agencies. At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.'},
        ];

        const skills = ['Web development', 'jQuery', 'AngularJS', 'HTML5', 'CSS3', 'SCSS', 'gulp', 'Razor'];

        return (
            <div className="cvList">
                <h3>Education</h3>
                <ul className="eventList">
                    {
                        educationEvents.map(function(item, index){
                            return <EducationEvent key={index} itemId={'educationEvent' + index} title={item.title} date={item.date} details={item.details}/>
                        })
                    }
                </ul>

                <h3>Employment</h3>
                <ul className="eventList">
                    {
                        employmentEvents.map(function(item, index){
                            return <EmploymentEvent key={index} itemId={'employmentEvent' + index} title={item.title} date={item.date} details={item.details} company={item.company}/>
                        })
                    }
                </ul>

                <h3>Projects</h3>
                <ul className="eventList">
                    {
                        projects.map(function(item, index){
                            return <ProjectEvent key={index} itemId={'projectEvent' + index} title={item.title} date={item.date} details={item.details}/>
                        })
                    }
                </ul>

                <h3 className="skillsListHeader">Skills</h3>
                <ul className="skillsList">
                    {
                        skills.map(function(skill, index){
                            return <Skill key={index} skill={skill}/>
                        })
                    }
                </ul>
            </div>
        )
    }
}

export default CVList;