import React from 'react';

//Class used to render introduction to CV
class CVIntro extends React.Component {
    render() {
        //Temp data sources
        const person = {
            firstName       : "Maria",
            lastName        : "Lind",
            introText       : "I’m a frontend developer working with HTML5, CSS3 (Sass), JavaScript (jQuery, AngularJS) and Umbraco CMS (both Razor and XSLT). I have worked with responsive design and mobile specific websites. I primarily operate within a .NET environment and MVC pattern. Privately, my philosophy of lifelong learning takes me to the many corners of human knowledge. I am a social media addict, I read avidly and I play board games. I enjoy challenges and solving puzzles. And whenever the time allows it, I travel.",
            img             : "/Maria Lind.jpg"
        };

        return (
            <div className="intro">
                <h1><img src={person.img} alt={person.firstName + ' ' + person.lastName}/> {person.firstName} {person.lastName}</h1>
                <p>{person.introText}</p>
            </div>
        )
    }
}

export default CVIntro;