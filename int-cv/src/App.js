import React from 'react';
import CVList from './CVList';
import CVIntro from './CVIntro';
import CVContact from './CVContact';
import './css/normalize.css';
import './css/style.css';

//provides top HTML scaffolding for CV
//makes use of various components
class App extends React.Component {
    render() {
        return (
            <div className="container">
                <div className="header">
                    <CVIntro />
                </div>
                <div className="main">
                    <CVList />
                </div>
                <div className="contact">
                    <CVContact />
                </div>
            </div>
        );
    }
}

export default App;
